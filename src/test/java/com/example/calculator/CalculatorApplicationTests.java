package com.example.calculator;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.ui.Model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class CalculatorApplicationTests{

	@Test
	public void testAddition() {
		CalculatorController calculatorController = new CalculatorController();
		Model model = mock(Model.class);

		String result = calculatorController.calculate(2.0, 3.0, "add", model);

		assertEquals("calculator", result);
		verify(model).addAttribute(eq("result"), eq("Result: 5.0"));
	}

	@Test
	public void testSubtraction() {
		CalculatorController calculatorController = new CalculatorController();
		Model model = mock(Model.class);

		String result = calculatorController.calculate(5.0, 3.0, "subtract", model);

		assertEquals("calculator", result);
		verify(model).addAttribute(eq("result"), eq("Result: 2.0"));
	}

	@Test
	public void testMultiplication() {
		CalculatorController calculatorController = new CalculatorController();
		Model model = mock(Model.class);

		String result = calculatorController.calculate(5.0, 3.0, "multiply", model);

		assertEquals("calculator", result);
		verify(model).addAttribute(eq("result"), eq("Result: 15.0"));
	}

	@Test
	public void testDivision() {
		CalculatorController calculatorController = new CalculatorController();
		Model model = mock(Model.class);

		String result = calculatorController.calculate(6.0, 3.0, "divide", model);

		assertEquals("calculator", result);
		verify(model).addAttribute(eq("result"), eq("Result: 2.0"));
	}

	@Test
	public void testDivisionByZero() {
		CalculatorController calculatorController = new CalculatorController();
		Model model = mock(Model.class);

		String result = calculatorController.calculate(6.0, 0.0, "divide", model);

		assertEquals("calculator", result);
		verify(model).addAttribute(eq("result"), eq("Result: NaN"));
	}
}
